# Interlock

A collection of interlock notes and technique.
Stolen from other fonts.

Is an _interlock_ different from a _ligature_?
What about _intertwining_?

(See Synthesis below for partial thoughts)


## To ingest

- Benguiat Interlock https://fontsinuse.com/typefaces/125579/benguiat-interlock
- Benguiat Newlock https://fontsinuse.com/typefaces/125578/benguiat-newlock
- Ed Interlock https://fontsinuse.com/typefaces/40498/ed-interlock
- Swung Note https://fontsinuse.com/typefaces/29098/swung-note
- Sundowners https://fontsinuse.com/typefaces/28977/sundowners
- The Lightning Fingers of Roy Clark
  https://www.discogs.com/master/383136-Roy-Clark-The-Lightning-Fingers-Of-Roy-Clark/image/SW1hZ2U6NDQyMjgwMzc=
  (thanks Florian!)
- Custom job using ITC Honda https://fontsinuse.com/uses/31443/lighthouse-one-fine-morning-hats-off-to-the-s
- Custom job using Marvin https://fontsinuse.com/uses/18726/a-trace-of-memory-by-keith-laumer-paperback-l

Ed Benguiat is the designer (or co-) of 3 of these.
Benguiat Interlock came first (1965?), with Benguiat Newlock as
a revision.
Ed Interlock is an do-over recruiting Tal Leming to do the
OpenType features.

Of these, the modern Ed Interlock has the largest number of
showings on FontsInUse (5).


## Romanisch

The [font
Romanisch](https://fontsinuse.com/typefaces/44490/romanisch) has several ligatures; [the Specimen shows
several](https://www.flickr.com/photos/n1ke/4856803704/in/album-72157624519443005/)


![Romanisch](RomanischLigature.png "Romanisch")

Many of the ligatures enlarge, drop, or raise the basic shape to
create an ascending, descending, or extending capital letter.

**AU** intertwines.
The **A** is raised on the right,
the vertical left-hand stem of **U** is truncated.

The **C** is enlarged and descends; the enlarged opening
consumes the left-hand stem of **H** or **K**, which is
truncated to fit.

The **E** has an extended leg (bottom horizontal stroke), which
can fit under:
a raised **I**;
a raised **U** with truncated left-hand stem;
an **N** or **R** with a truncated left-hand stem;

**FT** has an **F** that has been extended up so the crossbar of
**T** locks in.

The **L** descends and its leg tucks under **A** **E** **I** and
a slightly diminished **O**.
The **LT** ligature has an unmodified **L**, but raises the
**T** (possibly slightly diminished) to sit on the leg of the
**L**.

**PH** intertwines.
The entire loop of **P** is enlarged to receive the truncated
left-hand stem of **H**.

The **UE** ligature is common older form of umlaut: the **E** is
made much smaller to fit in the counter of **U**.
The same specimen page (not shown) shows that the regular umlaut
using a diaeresis is available.

**SS** intertwines.
One **S** descends, the other ascends.

In **ST**, the **S** is extended and opened vertically,
greatly opening both counters, allowing the top counter to
receive the cross of the **T**.
The lower-left stroke of **S** now finishes pointing at the 8
o’clock position (more like 10 or 11 o’clock in the regular
**S**).

For this font (which i think was made available in the
German-speaking market first) the ligatures are presumably
linguistically motivated.
For example **EH** and **LU** would both be possible using the
ideas from the existing ligatures, but are not common pairs in
German so wouldn't get used.

Ligatures for a German language target also suit English.


## Song of Solomon

The jacket for [Song of Solomon by Toni
Morrison](https://fontsinuse.com/uses/27323/song-of-solomon-by-toni-morrison-alfred-a-kno)
has many features, some of which could be typographic
interlocking, others are very definitely custom graphic design
(using ITC Serif Gothic).


![Solomon](Solomon.png "Solomon")

The **OLO** features an extended **L** that now receives the
next **O**.
Both **O**s are touching the **L** (the text is set
touching as much as possible).

The **SO** has a _greatly_ enlarged **S** with a modified top
counter that receives the **O**.
It would be possible to do something like this as an interlock
ligature, but it does not seem advisable.
Would be an amusing way to set `MinY` though.


## Koziar Interlock

Koziar Interlock provides many interlock examples (as do other
"Interlock" fonts).
The general style of Koziar Interlock is an informal graphic
sans serif with flags (or discretionary slab serifs).

![Remote Control](RemoteControl.png "Remote Control")

The [Remote Control
showing](https://fontsinuse.com/uses/39327/mtv-s-remote-control)
has ligature / interlocks that generally consist of a diminished
letter being pushed under an extended horizontal stroke:

We see **E** **O** **R** in dimished forms (approximately
smallcaps, if the font had them) fitting underneath the extended
left crossbar of **T**, the right crossbar of **T**, and an
extremely extended left-facing flag on the top-left of **M** and
**N**.

We also see an **N** with a truncated right-hand stem fitting
underneath the crossbar of **T**.

The [Doctor to the Stars] showing has a few more examples of
fitting letters under the **T**:

![Doctor to the Stars](DoctorToTheStars.png "Doctor to the Stars")

In **TA** the **A** is diminished to fit under the **T**.
There is also a **TH** ligature, in a more conventional form,
rather than an interlock.

In this font the **T** seems to have no balanced form;
all showings have it either extended on the left or the right.
Many letters have flags (i don't really want to call them
serifs), which encourages the fitting-under designs.
Here we see flags on **D** **R** and **A**.

A couple of the design elements of Koziar Interlock help the
interlocks:
flags, as previously mentioned;
uneven weight / balance, examples of which can be seen in the
vertical stem of **R** and **E** and the crossbar of **T** (and
others).

The uneven weight disrupts the idea that everything in the font
should be balanced and even, which makes it easier to introduce
extended flags and so on which definitely are not balanced.

Contrast this with Romanisch (above) which is more harmonised.


## Mantinia

A design derived from and inspired by stonecut lettering.

The best examples on FontsInUse are:
- [Carter Stamps](https://fontsinuse.com/uses/6858/matthew-carter-mailing-stamps)
- [Queen Jade](https://fontsinuse.com/uses/940/the-queen-jade-by-yxta-maya-murray)
- [Michel de Montaigne](https://fontsinuse.com/uses/1195/the-autobiography-of-michel-de-montaigne)

The Carter Stamps are the cutest.
Matthew Carter designed his own stamps using his own font.

![First Class](FirstClass.png "First Class")

Both the examples on this stamp are a leg-under.
The **R** has its leg extended to fit under a small-cap **S**.
In **CLASS** the leg of the **L** fits under a small-cap **A**.
In both cases the following letter is also kerned to fit.

Compared to Koziar Interlock or Ed Interlock, Mantinia has a
lighter touch. And, like Romanisch, is more harmonious.

Supplying small-caps raised allows a number of fairly natural
leg-under type interlocks.
Not shown is a semi-interlock with **AI**: Capital **A**
followed by raised small-cap **I**.
In a way we can think of the regular, baseline, small-caps
giving rise to "natural" interlocks when they are kerned with
letters like **T**.

The Queen Jade example is a sort of minimal, not actually
interlocking interlock:
The capital **Q** has a descending tail that extends so far to
the right that it reaches under the next two letters.
It also shows that it might be interesting and useful to use
raised small-caps even when they're not interlocking.

The Michel de Montaigne example shows something that is more of
a ligature than an interlock, but in fact is really an emulation
of more traditional stonecutting.

![Marvin](Marvin.png "Marvin")

**VI** in **MARVIN** has been replaced with a vertically stacked
ligature, a diminished (small-cap size?) **I** slotting into the
opening of the **V**.

The font has many more ligature inspired by stonecutting (for
example **TE** **LA**) that i don't consider here as interlocks.


## Synthesis

The most abstract synthesis is that interlocking consists of
modifying form so that spaces can receive adjacent shapes or
parts of shapes.

(that definition may be useful to distinguish interlocks from
ligatures)

The common, easy mode, interlocks are tucking a leg under or an
arm over: extending the leg of an **E** **L** **R** to go under
the next letter, either by descending or by modifying the next
letter (or both); extending the arm of **T** to stretch over the
previous or next letter, again either by ascending or by
modifying the adjacent letter (or both).

Koziar Interlock shows how flags can become arms.

Romanisch has another trick: the **C** can be expanded to make
its counter big enough to receive a letter.

# END
